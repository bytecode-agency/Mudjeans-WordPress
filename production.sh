#!/bin/sh
rm -f .babelrc .esdoc.json .eslintrc .gitignore .postcssrc .travis.yml LICENSE.md package.json README.md yarn.lock
echo 'Cleaned development files in root folder'

rm -rf src
echo 'Removed src folder'

rm -rf .git
echo 'Removed .git folder'

echo "It's all clear. You're good to go, after you remove this file too!"