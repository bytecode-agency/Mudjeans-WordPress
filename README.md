# Mudjeans WordPress/WooCommerce

[![dependencies Status](https://david-dm.org/nooijensolutions/Mudjeans-WordPress/status.svg)](https://david-dm.org/nooijensolutions/Mudjeans-WordPress)
[![devDependencies Status](https://david-dm.org/nooijensolutions/Mudjeans-WordPress/dev-status.svg)](https://david-dm.org/nooijensolutions/Mudjeans-WordPress?type=dev)
[![GPL Licence](https://badges.frapsoft.com/os/gpl/gpl.svg?v=103)](https://opensource.org/licenses/GPL-3.0/)
[![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

This is the WordPress/WooCommerce theme for Mudjeans.

## Dependencies

Development:

* Yarn (not NPM)
* Node
* Local WordPress installation

Production:

* WordPress server
  * PHP (5.6+)
  * MySQL or other WordPress compatible SQL server

To install all dependencies (needed for both development and production build), run:

```sh
yarn
```

## Development

For development, the Parcel Package Bundler is used. To run the development server, run the following command:

```sh
yarn run dev
```

The Parcel Bundler is now listening. For your dev server, spin up a local WordPress installation in the root of this folder.

## Production build

For making the production build, run the following command:

```sh
yarn run build
```

When the theme is installed on the server, run the following command (while in the root directory of the theme):

```sh
sh production.sh && rm -f production.sh
```

**Make sure you have separate development and production environments!**

## Contributors

* Luciano Nooijen
* Jeroen van Steijn