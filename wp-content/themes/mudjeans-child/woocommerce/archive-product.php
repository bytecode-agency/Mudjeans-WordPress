<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'shop' );

/**
 * Change title to the ACF title with this hook
 */
remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title', 10 );
add_action('woocommerce_shop_loop_item_title', 'abChangeProductsTitle', 10 );
function abChangeProductsTitle() {
	global $product;
	$id = $product->get_id();
	echo '<h2 class="woocommerce-loop-product_title"><a href="'.get_the_permalink().'">' . the_field('product_titel', $id) . '</a></h2>';
}

/**
 * Remove add to cart from archive & Add ACF values
 */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
add_action('woocommerce_shop_loop_item_title', 'bzAddColor', 10 );
function bzAddColor(){
	global $product;
	$id = $product->get_id();
	$color_name = get_field('colour_name',$id);
	$color_hex = get_field('colour_picker',$id);
	echo '<ul><li class="subtitle">'.$color_name.'</li>';
	echo '<li class="color"><span class="color-circle" style="background-color: '.$color_hex.';"></span></li>';
	echo '</ul>';
}
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

/**
 * Hook for rollover image on archive page wrapper
 */
add_action( 'woocommerce_before_shop_loop_item_title', 'addWrapperRollover', 5, 2);
add_action( 'woocommerce_before_shop_loop_item_title',create_function('', 'echo "</div>";'), 12, 2);

/**
 * Thumbnail size
 */
add_filter( 'woocommerce_gallery_thumbnail_size', function( $size ) {
    return 'thumbnail';
} );

/**
 * Another attempt at thumbnail size
 */
add_filter( 'woocommerce_gallery_thumbnail_size', 'custom_woocommerce_gallery_thumbnail_size' );
function custom_woocommerce_gallery_thumbnail_size() {
  return 'woocommerce_thumbnail';
}
 
function addWrapperRollover(){
	?>
<div class="archive-img-wrap backgroundsize" style="background-image: url('<?php 
	global $post;
	echo the_field('hover_image',$post->ID); ?>');">
	<?php
}

?>
<style>
@media screen and (min-width: 768px){
	#main{
		display:  grid;
		grid-template-columns: 2fr 5fr;
		grid-template-areas: 
		"sidebar products"
		". pagination"
	}

	#main > .products{
		grid-area: products;
	}

	aside{
		grid-area: sidebar;
		padding-right: 30px;
		margin-left: 10%;
		margin-top: 27px;
	}

	aside h3 {
		padding-bottom: 1.5em !important;
	}

	.storefront-sorting{
		grid-area: pagination;
	}

	.col-full{
		max-width: 70%;
	}
	.woocommerce {
		margin-top: 60px;
	}
	.sub-menu {
		margin-left: 6px !important;
	}
}
</style>
<aside class="filter">
	<nav class="filter_nav">
	<?php 

	global $wp_query;
	
	$current_termid = $wp_query->get_queried_object()->term_id; 
	$parents = get_ancestors($current_termid, 'product_cat');

	$menID = 185;
	$womenID = 186;
			
	if ( is_product_category('men') || in_array( $menID, $parents ) ) {
		dynamic_sidebar('sidebar-men');
	} elseif( is_product_category('women') || in_array( $womenID, $parents ) ) {
		dynamic_sidebar('sidebar-women');
	} else {
		// doe niets
	}
	?>
	</nav>
</aside>
<?php
if ( have_posts() ) {

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		?>
		<div class="highlight_photo">
		<header class="highlight_header">
			<div class="flex">
				<div class="the_title no-float width-unset inline-block">

					<?php 

					global $wp_query;
					    $terms_post = get_the_terms( $post->cat_ID , 'product_cat' );
					    foreach ($terms_post as $term_cat) { 
					    
					    $term_name = $term_cat->name;
					    $term_id = $term_cat->term_id;
					}

					$thumbnail_id = get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );
					$image = wp_get_attachment_url( $thumbnail_id );

					?>
					<h3><?php echo $term_name; ?></h3>
				</div>
				<div class="the_info no-float width-unset flex-fill" style="padding-left: 28px;">
					<h4 style="margin: 0;">Slim fit. High waist. Zip Fly</h4>
					<ul style="margin: 0;">
						<li class="subtitle">Available in 6 colours</li>
						<li class="color"><span style="background-color:#0b4473;"></span></li>
						<li class="color"><span style="background-color:#558fb4;"></span></li>
						<li class="color"><span style="background-color:#010407;"></span></li>
						<li class="color"><span style="background-color:#59809c;"></span></li>
						<li class="color"><span style="background-color:#010407;"></span></li>
						<li class="color"><span style="background-color:#59809c;"></span></li>
					</ul>
				</div>
			</div>
		</header>
		
		<?php if (!empty($thumbnail_id)): ?>
			<div class="the_image">
				<figure class="visual"><img src="<?php echo $image; ?>" alt=""></figure>
			</div>	
		<?php endif; ?>
		
	</div>

		<?
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 *
			 * @hooked WC_Structured_Data::generate_product_data() - 10
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
