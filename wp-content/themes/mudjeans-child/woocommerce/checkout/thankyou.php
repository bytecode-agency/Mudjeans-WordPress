<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>



<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
				<li class="woocommerce-order-overview__order order">
					<p><strong><?php _e( 'Order number: #', 'woocommerce' ); ?><?php echo $order->get_order_number(); ?></strong></p>
					<h2>Thanks <?php echo get_post_meta($order->ID,'_billing_first_name', true); ?>!</h2>
				</li>
			</ul>


			<div id="map" style="width:100%; height: 400px; margin-bottom: 40px;"></div>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'We have accepted your order and we are preparing it. Visit this page again for up-dates on your shipping status.', 'woocommerce' ), $order ); ?></p>
			
		
				<?php  

				$address = $order->get_address( 'shipping' );
				
				?>	


			
			<script>

			     function initMap() {

			     	var geocoder= new google.maps.Geocoder();

			     	geocoder.geocode( { 'address': '<?php echo $address['city']; ?>'}, function(results, status) {
			     	  if (status == google.maps.GeocoderStatus.OK)
			     	  {
			     	    
			     	      var lat = results[0].geometry.location.lat()
			     	      var long = results[0].geometry.location.lng();

			     	      var loc = {lat: lat, lng: long};

			     	      var map = new google.maps.Map(document.getElementById('map'), {
			     	        zoom: 13,
			     	        center: loc
			     	      });

			     	      var contentString = '<div id="content">'+
			     	                  '<p><?php echo $address['city']; ?></p>'+
			     	                  '</div>';

			     	              var infowindow = new google.maps.InfoWindow({
			     	                content: contentString
			     	              });

			     	      var marker = new google.maps.Marker({
			     	        position: loc,
			     	        map: map
			     	      });


			     	      marker.addListener('click', function() {
			     	               infowindow.open(map, marker);
			     	             });

			     	  }
			     	});


			      

			     }
			   </script>
			   <script async defer
			      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJe_srDiuwtppikNYdC50gZYExwZOdxxo&callback=initMap">
			   </script>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>


</div>
