<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

?>
<div class="color-review-fit">
<div class="form_row">
	<label for="c1"><h3>Select colour</h3></label>
	<div class="variants">
		<?php 
		$posts = get_field('other_colours');
		if( $posts ): ?>
			<?php foreach( $posts as $p ):?>
				<a href="<?php echo(get_the_permalink($p->ID)); ?>"><button class="round-button" style="background-color: <?php the_field('colour_picker',$p->ID) ?>"></button></a>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<p class="the_name"><?php the_field('colour_name') ?></p>
</div>
<div class="the_reviews">
	<div class="current_reviews">
		<ul>
			<li>1</li>
			<li>2</li>
			<li>3</li>
			<li>4</li>
			<li>5</li>
		</ul>
		<p><a href="#" title="">(3 customers review)</a></p>
	</div>
	<p><a href="#" title="">find your fit</a></p>
</div>
<div class="detail tabs">
	<nav class="tabnav">
		<ul class="tab-links">
			<li class="active"><a href="#details" title="">Details</a></li>
			<li><a href="#description" title="">Description</a></li>
			<li class=""><a href="#shipping" title="">Shipping</a></li>
		</ul>
	</nav>
	<div class="the_tabs">
		<div class="tab-content">
			<div class="tab" id="description">
				<?php the_field('description') ?>
			</div>
			
			<div class="tab active" id="details">
				<?php the_field('details') ?>
			</div>
			
			
			<div class="tab" id="shipping">
				<?php the_field('shipping') ?>
			</div>
		</div>
	</div>
</div>
</div>