<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>
	</div>
	</div>
	</div>
	<h2 style="font-size: 18px; text-align: center;"><?php esc_html_e( 'Similar products you might like', 'woocommerce' ); ?></h2>
	<section class="products">
		<div class="similar_products ______row">
		<?php 
		$posts = get_field('similar_products');
		if( $posts ): ?>
			<?php foreach( $posts as $p): ?>
			<?php 
			$_product = wc_get_product( $p->ID );
			?>
				<article class="product_item">
					<a href="<?php echo(get_permalink($p->ID)); ?>" title="">
						<div class="the_visual">
							<figure class="visual visible"><img src="<?php 
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $p->ID ), 'single-post-thumbnail' );
							echo $image[0];
							?>" alt=""></figure>
							<figure class="visual backgroundsize" style="background-image:url('<?php the_field('hover_image',$p->ID) ?>');"></figure>
						</div>
						<div class="the_content">
							<h3><?php the_field('product_titel', $p->ID); ?></h3>
							<p><?php echo(wc_price($_product->get_price())); ?></p>
						</div>
					</a>
					<ul>
						<li class="subtitle"><?php the_field('colour_name', $p->ID) ?></li>
						<li class="color"><span style="background-color:<?php the_field('colour_picker', $p->ID) ?>;"></span></li>
					</ul>
				</article>
    	<?php endforeach; ?>
		<?php endif; ?>
	</div>
	</section>

<?php endif;

wp_reset_postdata();
