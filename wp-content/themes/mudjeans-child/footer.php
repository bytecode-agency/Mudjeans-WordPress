<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Mudjeans
 */

?>

	</div><!-- #content -->
		<footer id="footer">
			<div class="footer_columns">
				<div class="center">
					<div class="the_connection">
						<div class="the_newsletter">
							<?php echo do_shortcode('[mc4wp_form id="78346"]'); ?>
						</div>
						<div class="social">
							<ul>
								<li class="instagram"><a href="#" title="">Instagram</a></li>
								<li class="facebook"><a href="#" title="">Facebook</a></li>
								<li class="pinterest"><a href="#" title="">Pinterest</a></li>
								<li class="twitter"><a href="#" title="">Twitter</a></li>
							</ul>
						</div>
					</div>
					<div class="the_columns">
						<?php if ( is_active_sidebar( 'footer-sidebar-1' ) ) : ?>
						<article class="footer_content">
							<?php dynamic_sidebar( 'footer-sidebar-1' ); ?>
						</article>
						<?php endif; ?>
						<?php if ( is_active_sidebar( 'footer-sidebar-2' ) ) : ?>
						<article class="footer_content">
							<?php dynamic_sidebar( 'footer-sidebar-2' ); ?>
						</article>
						<?php endif; ?>
						<?php if ( is_active_sidebar( 'footer-sidebar-3' ) ) : ?>
						<article class="footer_content">
							<?php dynamic_sidebar( 'footer-sidebar-3' ); ?>
						</article>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="payment">
				<div class="center">
					<ul>
						<li><img src="/wp-content/themes/mudjeans-child/assets/images/ideal.jpg" alt=""></li>
						<li><img src="/wp-content/themes/mudjeans-child/assets/images/mastercard.jpg" alt=""></li>
						<li><img src="/wp-content/themes/mudjeans-child/assets/images/paypal.jpg" alt=""></li>
						<li><img src="/wp-content/themes/mudjeans-child/assets/images/sofort.jpg" alt=""></li>
						<li><img src="/wp-content/themes/mudjeans-child/assets/images/visa.jpg" alt=""></li>
					</ul>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<nav id="mobile" class="hidden" style="padding-top: 100px;" aria-hidden="true">	
		<?php wp_nav_menu(array('container'=> '','theme_location'=>'primary')); ?>
	</nav>
</div><!-- #page -->

<?php wp_footer(); ?>
		
<script src="/wp-content/themes/mudjeans-child/assets/js/slick.js"></script>
<script src="/wp-content/themes/mudjeans-child/assets/js/base.js"></script>
	
	<!-- Start of LiveChat (www.livechatinc.com) code -->
	<script type="text/javascript">
	window.__lc = window.__lc || {};
	window.__lc.license = 7952591;
	(function() {
	  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
	  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
	  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
	})();
	</script>
	<!-- End of LiveChat code -->

</body>
</html>