<?php /* Template Name: Blog-overzicht */ ?>
<?php get_header(); ?>
			<section class="the_page">
				<div class="blog_view">
					<div class="center">
						<div class="blog_list">
							<div class="small_items">
						<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page'         => '10',
						);
						$post_query = new WP_Query($args);
						if($post_query->have_posts() ) {
							$counter = 1;
							while($post_query->have_posts() ) { 
								$post_query->the_post();
								if($counter % 5 == 0){?>
								</div>
									<!-- Big item -->
									<div class="large_item">
										<article class="blog_item">
											<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
												<figure class="backgroundsize visual" style="background-image:url(<?php the_post_thumbnail_url(); ?>);"></figure>
												<div class="the_content">
													<h2 class="title"><?php the_title(); ?></h2>
													<time><?php echo mysql2date( get_option( 'date_format' ), $post->post_date); ?></time>
													<?php the_excerpt(); ?>
												</div>
											</a>
										</article>
									</div>
									</div>
									<div class="blog_list">
									<div class="small_items">

								<?php } else {?>
									<!-- Small item -->
									<article class="blog_item">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
											<figure class="backgroundsize visual" style="background-image:url(<?php the_post_thumbnail_url(); ?>);"></figure>
											<div class="the_content">
												<h2 class="title"><?php the_title(); ?></h2>
												<time><?php echo mysql2date( get_option( 'date_format' ), $post->post_date); ?></time>
												<?php the_excerpt(); ?>
											</div>
										</a>
									</article>
								<?
								}
								$counter++;
							}
						}
						?>
							</div>
						</div>
					</div>
				</div>
			</section>	
			
<?php get_footer(); ?>