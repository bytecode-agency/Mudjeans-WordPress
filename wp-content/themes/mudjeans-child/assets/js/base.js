window.Modernizr=function(e,t,n){function r(e){h.cssText=e}function o(e,t){return typeof e===t}function i(e,t){return!!~(""+e).indexOf(t)}function a(e,t){for(var r in e){var o=e[r];if(!i(o,"-")&&h[o]!==n)return"pfx"!=t||o}return!1}function c(e,t,r){for(var i in e){var a=t[e[i]];if(a!==n)return!1===r?e[i]:o(a,"function")?a.bind(r||t):a}return!1}function l(e,t,n){var r=e.charAt(0).toUpperCase()+e.slice(1),i=(e+" "+b.join(r+" ")+r).split(" ");return o(t,"string")||o(t,"undefined")?a(i,t):(i=(e+" "+E.join(r+" ")+r).split(" "),c(i,t,n))}var s,u,f={},d=t.documentElement,p="modernizr",m=t.createElement(p),h=m.style,v=t.createElement("input"),y=":)",g=" -webkit- -moz- -o- -ms- ".split(" "),b="Webkit Moz O ms".split(" "),E="Webkit Moz O ms".toLowerCase().split(" "),w={svg:"http://www.w3.org/2000/svg"},x={},C={},S={},j=[],T=j.slice,k=function(e,n,r,o){var i,a,c,l,s=t.createElement("div"),u=t.body,f=u||t.createElement("body");if(parseInt(r,10))for(;r--;)(c=t.createElement("div")).id=o?o[r]:p+(r+1),s.appendChild(c);return i=["&#173;",'<style id="s',p,'">',e,"</style>"].join(""),s.id=p,(u?s:f).innerHTML+=i,f.appendChild(s),u||(f.style.background="",f.style.overflow="hidden",l=d.style.overflow,d.style.overflow="hidden",d.appendChild(f)),a=n(s,e),u?s.parentNode.removeChild(s):(f.parentNode.removeChild(f),d.style.overflow=l),!!a},N=function(){var e={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return function(r,i){i=i||t.createElement(e[r]||"div");var a=(r="on"+r)in i;return a||(i.setAttribute||(i=t.createElement("div")),i.setAttribute&&i.removeAttribute&&(i.setAttribute(r,""),a=o(i[r],"function"),o(i[r],"undefined")||(i[r]=n),i.removeAttribute(r))),i=null,a}}(),M={}.hasOwnProperty;u=o(M,"undefined")||o(M.call,"undefined")?function(e,t){return t in e&&o(e.constructor.prototype[t],"undefined")}:function(e,t){return M.call(e,t)},Function.prototype.bind||(Function.prototype.bind=function(e){var t=this;if("function"!=typeof t)throw new TypeError;var n=T.call(arguments,1),r=function(){if(this instanceof r){var o=function(){};o.prototype=t.prototype;var i=new o,a=t.apply(i,n.concat(T.call(arguments)));return Object(a)===a?a:i}return t.apply(e,n.concat(T.call(arguments)))};return r}),x.touch=function(){var n;return"ontouchstart"in e||e.DocumentTouch&&t instanceof DocumentTouch?n=!0:k(["@media (",g.join("touch-enabled),("),p,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(e){n=9===e.offsetTop}),n},x.backgroundsize=function(){return l("backgroundSize")},x.cssanimations=function(){return l("animationName")},x.csstransforms=function(){return!!l("transform")},x.csstransforms3d=function(){var e=!!l("perspective");return e&&"webkitPerspective"in d.style&&k("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(t,n){e=9===t.offsetLeft&&3===t.offsetHeight}),e},x.csstransitions=function(){return l("transition")},x.generatedcontent=function(){var e;return k(["#",p,"{font:0/0 a}#",p,':after{content:"',y,'";visibility:hidden;font:3px/1 a}'].join(""),function(t){e=t.offsetHeight>=3}),e},x.video=function(){var e=t.createElement("video"),n=!1;try{(n=!!e.canPlayType)&&((n=new Boolean(n)).ogg=e.canPlayType('video/ogg; codecs="theora"').replace(/^nojQuery/,""),n.h264=e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^nojQuery/,""),n.webm=e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^nojQuery/,""))}catch(e){}return n},x.svg=function(){return!!t.createElementNS&&!!t.createElementNS(w.svg,"svg").createSVGRect};for(var z in x)u(x,z)&&(s=z.toLowerCase(),f[s]=x[z](),j.push((f[s]?"":"no-")+s));return f.input||(f.input=function(n){for(var r=0,o=n.length;r<o;r++)S[n[r]]=!!(n[r]in v);return S.list&&(S.list=!(!t.createElement("datalist")||!e.HTMLDataListElement)),S}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),f.inputtypes=function(e){for(var r,o,i,a=0,c=e.length;a<c;a++)v.setAttribute("type",o=e[a]),(r="text"!==v.type)&&(v.value=y,v.style.cssText="position:absolute;visibility:hidden;",/^rangejQuery/.test(o)&&v.style.WebkitAppearance!==n?(d.appendChild(v),r=(i=t.defaultView).getComputedStyle&&"textfield"!==i.getComputedStyle(v,null).WebkitAppearance&&0!==v.offsetHeight,d.removeChild(v)):/^(search|tel)jQuery/.test(o)||(r=/^(url|email)jQuery/.test(o)?v.checkValidity&&!1===v.checkValidity():v.value!=y)),C[e[a]]=!!r;return C}("search tel url email datetime date month week time datetime-local number range color".split(" "))),f.addTest=function(e,t){if("object"==typeof e)for(var r in e)u(e,r)&&f.addTest(r,e[r]);else{if(e=e.toLowerCase(),f[e]!==n)return f;t="function"==typeof t?t():t,d.className+=" "+(t?"":"no-")+e,f[e]=t}return f},r(""),m=v=null,function(e,t){function n(e,t){var n=e.createElement("p"),r=e.getElementsByTagName("head")[0]||e.documentElement;return n.innerHTML="x<style>"+t+"</style>",r.insertBefore(n.lastChild,r.firstChild)}function r(){var e=v.elements;return"string"==typeof e?e.split(" "):e}function o(e){var t=h[e[p]];return t||(t={},m++,e[p]=m,h[m]=t),t}function i(e,n,r){if(n||(n=t),s)return n.createElement(e);r||(r=o(n));var i;return!(i=r.cache[e]?r.cache[e].cloneNode():d.test(e)?(r.cache[e]=r.createElem(e)).cloneNode():r.createElem(e)).canHaveChildren||f.test(e)||i.tagUrn?i:r.frag.appendChild(i)}function a(e,t){t.cache||(t.cache={},t.createElem=e.createElement,t.createFrag=e.createDocumentFragment,t.frag=t.createFrag()),e.createElement=function(n){return v.shivMethods?i(n,e,t):t.createElem(n)},e.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+r().join().replace(/[\w\-]+/g,function(e){return t.createElem(e),t.frag.createElement(e),'c("'+e+'")'})+");return n}")(v,t.frag)}function c(e){e||(e=t);var r=o(e);return!v.shivCSS||l||r.hasCSS||(r.hasCSS=!!n(e,"article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),s||a(e,r),e}var l,s,u=e.html5||{},f=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)jQuery/i,d=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)jQuery/i,p="_html5shiv",m=0,h={};!function(){try{var e=t.createElement("a");e.innerHTML="<xyz></xyz>",l="hidden"in e,s=1==e.childNodes.length||function(){t.createElement("a");var e=t.createDocumentFragment();return void 0===e.cloneNode||void 0===e.createDocumentFragment||void 0===e.createElement}()}catch(e){l=!0,s=!0}}();var v={elements:u.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:"3.7.0",shivCSS:!1!==u.shivCSS,supportsUnknownElements:s,shivMethods:!1!==u.shivMethods,type:"default",shivDocument:c,createElement:i,createDocumentFragment:function(e,n){if(e||(e=t),s)return e.createDocumentFragment();for(var i=(n=n||o(e)).frag.cloneNode(),a=0,c=r(),l=c.length;a<l;a++)i.createElement(c[a]);return i}};e.html5=v,c(t)}(this,t),f._version="2.8.3",f._prefixes=g,f._domPrefixes=E,f._cssomPrefixes=b,f.hasEvent=N,f.testProp=function(e){return a([e])},f.testAllProps=l,f.testStyles=k,f.prefixed=function(e,t,n){return t?l(e,t,n):l(e,"pfx")},d.className=d.className.replace(/(^|\s)no-js(\s|jQuery)/,"jQuery1jQuery2")+" js "+j.join(" "),f}(this,this.document),function(e,t,n){function r(e){return"[object Function]"==v.call(e)}function o(e){return"string"==typeof e}function i(){}function a(e){return!e||"loaded"==e||"complete"==e||"uninitialized"==e}function c(){var e=y.shift();g=1,e?e.t?m(function(){("c"==e.t?d.injectCss:d.injectJs)(e.s,0,e.a,e.x,e.e,1)},0):(e(),c()):g=0}function l(e,n,r,o,i,l,s){function u(t){if(!p&&a(f.readyState)&&(b.r=p=1,!g&&c(),f.onload=f.onreadystatechange=null,t)){"img"!=e&&m(function(){w.removeChild(f)},50);for(var r in T[n])T[n].hasOwnProperty(r)&&T[n][r].onload()}}var s=s||d.errorTimeout,f=t.createElement(e),p=0,v=0,b={t:r,s:n,e:i,a:l,x:s};1===T[n]&&(v=1,T[n]=[]),"object"==e?f.data=n:(f.src=n,f.type=e),f.width=f.height="0",f.onerror=f.onload=f.onreadystatechange=function(){u.call(this,v)},y.splice(o,0,b),"img"!=e&&(v||2===T[n]?(w.insertBefore(f,E?null:h),m(u,s)):T[n].push(f))}function s(e,t,n,r,i){return g=0,t=t||"j",o(e)?l("c"==t?C:x,e,t,this.i++,n,r,i):(y.splice(this.i++,0,e),1==y.length&&c()),this}function u(){var e=d;return e.loader={load:s,i:0},e}var f,d,p=t.documentElement,m=e.setTimeout,h=t.getElementsByTagName("script")[0],v={}.toString,y=[],g=0,b="MozAppearance"in p.style,E=b&&!!t.createRange().compareNode,w=E?p:h.parentNode,p=e.opera&&"[object Opera]"==v.call(e.opera),p=!!t.attachEvent&&!p,x=b?"object":p?"script":"img",C=p?"script":x,S=Array.isArray||function(e){return"[object Array]"==v.call(e)},j=[],T={},k={timeout:function(e,t){return t.length&&(e.timeout=t[0]),e}};(d=function(e){function t(e){var t,n,r,e=e.split("!"),o=j.length,i=e.pop(),a=e.length,i={url:i,origUrl:i,prefixes:e};for(n=0;n<a;n++)r=e[n].split("="),(t=k[r.shift()])&&(i=t(i,r));for(n=0;n<o;n++)i=j[n](i);return i}function a(e,o,i,a,c){var l=t(e),s=l.autoCallback;l.url.split(".").pop().split("?").shift(),l.bypass||(o&&(o=r(o)?o:o[e]||o[a]||o[e.split("/").pop().split("?")[0]]),l.instead?l.instead(e,o,i,a,c):(T[l.url]?l.noexec=!0:T[l.url]=1,i.load(l.url,l.forceCSS||!l.forceJS&&"css"==l.url.split(".").pop().split("?").shift()?"c":n,l.noexec,l.attrs,l.timeout),(r(o)||r(s))&&i.load(function(){u(),o&&o(l.origUrl,c,a),s&&s(l.origUrl,c,a),T[l.url]=2})))}function c(e,t){function n(e,n){if(e){if(o(e))n||(f=function(){var e=[].slice.call(arguments);d.apply(this,e),p()}),a(e,f,t,0,s);else if(Object(e)===e)for(l in c=function(){var t,n=0;for(t in e)e.hasOwnProperty(t)&&n++;return n}(),e)e.hasOwnProperty(l)&&(!n&&!--c&&(r(f)?f=function(){var e=[].slice.call(arguments);d.apply(this,e),p()}:f[l]=function(e){return function(){var t=[].slice.call(arguments);e&&e.apply(this,t),p()}}(d[l])),a(e[l],f,t,l,s))}else!n&&p()}var c,l,s=!!e.test,u=e.load||e.both,f=e.callback||i,d=f,p=e.complete||i;n(s?e.yep:e.nope,!!u),u&&n(u)}var l,s,f=this.yepnope.loader;if(o(e))a(e,0,f,0);else if(S(e))for(l=0;l<e.length;l++)s=e[l],o(s)?a(s,0,f,0):S(s)?d(s):Object(s)===s&&c(s,f);else Object(e)===e&&c(e,f)}).addPrefix=function(e,t){k[e]=t},d.addFilter=function(e){j.push(e)},d.errorTimeout=1e4,null==t.readyState&&t.addEventListener&&(t.readyState="loading",t.addEventListener("DOMContentLoaded",f=function(){t.removeEventListener("DOMContentLoaded",f,0),t.readyState="complete"},0)),e.yepnope=u(),e.yepnope.executeStack=c,e.yepnope.injectJs=function(e,n,r,o,l,s){var u,f,p=t.createElement("script"),o=o||d.errorTimeout;p.src=e;for(f in r)p.setAttribute(f,r[f]);n=s?c:n||i,p.onreadystatechange=p.onload=function(){!u&&a(p.readyState)&&(u=1,n(),p.onload=p.onreadystatechange=null)},m(function(){u||(u=1,n(1))},o),l?p.onload():h.parentNode.insertBefore(p,h)},e.yepnope.injectCss=function(e,n,r,o,a,l){var s,o=t.createElement("link"),n=l?c:n||i;o.href=e,o.rel="stylesheet",o.type="text/css";for(s in r)o.setAttribute(s,r[s]);a||(h.parentNode.insertBefore(o,h),m(n,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))},Modernizr.addTest("placeholder",function(){return!!("placeholder"in(Modernizr.input||document.createElement("input"))&&"placeholder"in(Modernizr.textarea||document.createElement("textarea")))});





jQuery(document).ready(function(){
	
	
	jQuery(window).scroll(function() {    
	    var scroll = jQuery(window).scrollTop();
	
	    if (scroll >= 1) {
	        jQuery("#header").addClass("active");
	    } else {
	        jQuery("#header").removeClass("active");
	    }
	});	
	
	
	
	
	jQuery('#toggle').click(function(){
		
		if(jQuery(this).hasClass('active')){
			
			jQuery(this).removeClass('active');
			jQuery('#mobile').fadeOut(300);
			jQuery('html').removeClass('menu-open');
			
			
		} else {
			
			jQuery(this).addClass('active');
			jQuery('#mobile').fadeIn(300);
			jQuery('html').addClass('menu-open');
			
		}
		
		return false;
	});
	
	
	
	
	
	
	jQuery('#sizebtn').click(function(){
		
		if(jQuery(this).hasClass('active')){
			
			jQuery('.sizechart').fadeOut(300);
			jQuery(this).removeClass('active');
			
			
		} else {
			
			jQuery('.sizechart').fadeIn(300);
			jQuery(this).addClass('active');
			
			
		}
		
		return false;
		
		
	});
	
	
	
	jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
 
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
    
    
   	jQuery('.scroller').click(function(){
		jQuery('html, body').animate({

			scrollTop: jQuery(this).parent().next('.scroll_hook').offset().top
			
		}, 1000);
		
		return false;
	});	 
	


	jQuery( "#meta .shoppingcart" ).mouseover(function() {
	  jQuery( this ).find('.shoppingcart-popup').show();
	});
	
	jQuery( ".shoppingcart-popup" ).mouseout(function() {
	  console.log('mouseout');
	  jQuery( this ).hide();
	});




	jQuery(function($) {

	    $('a.button_lease_prod')

	    .on('click', function(e) {
	        e.preventDefault();

	        var $variations = $('.variations_form .variations .value select'),
	            $url = $(this).attr('data-url') + '?';

	        $.each($variations, function(key, el) {

	            $url += $(el).attr('data-attribute_name') + '=';
	            $url += $(el).find('option:selected').val();

	            if(key == ($variations.length - 1)) {
	                window.location.href = $url;
	            } else {
	                $url += '&';
	            }
	        });

	    });
	});
	
});



















