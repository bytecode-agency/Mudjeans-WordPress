<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
	if(is_home()){
		wp_dequeue_style( 'storefront-style' );
		wp_dequeue_style( 'storefront-woocommerce-style' );
	}
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

/*
 * CUSTOM PHP BELOW
 */

// Enqueue styles and scripts
add_action( 'wp_enqueue_scripts', 'buro_zero_styles');
function buro_zero_styles() {
    wp_enqueue_style('custom-style', get_stylesheet_directory_uri().'/buro-zero/main.css');
    // wp_deregister_script('jquery');
    wp_enqueue_script ( 'custom-script', get_stylesheet_directory_uri() . '/buro-zero/main.js', null, null, true);
}

function register_my_menu() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
  }
add_action( 'init', 'register_my_menu' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mudjeans_widgets_init() {
	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Sidebar Men',
		'id' => 'sidebar-men',
		'description' => 'Appears in the sidebar of the "men" categories',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Sidebar Women',
		'id' => 'sidebar-women',
		'description' => 'Appears in the sidebar of the "women" categories',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'mudjeans_widgets_init' );

/**
 * Change choose an option text
 */
add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'my_wc_filter_dropdown_args', 10 );
function my_wc_filter_dropdown_args( $args ) {
    $args['show_option_none'] = 'Waist / Length';
    return $args;
}

/**
 * Change add to cart text
 */
add_filter( 'woocommerce_product_single_add_to_cart_text', 'bryce_add_to_cart_text' );
function bryce_add_to_cart_text() {

		global $product;

		if( class_exists( 'WC_Subscriptions_Product' ) && WC_Subscriptions_Product::is_subscription( $product ) ) {
		    return __( 'Lease now', 'mudjeans-child' );
		} else {
		    return __( 'Buy now', 'mudjeans-child' );
		}

}


/**
 * Add lease button link
 */
add_action( 'woocommerce_single_product_summary',  'add_lease_button',  35 ); 
function add_lease_button() {

		global $product;
		$prod_id = $product->get_id();
		$linkedLeaseProd = get_field('lease_product', $prod_id);
		$url = get_permalink( $linkedLeaseProd->ID);

		$prod_variations = $product->get_variation_attributes();
		
		if (!empty($linkedLeaseProd)) {
			echo '<a class="button button_lease_prod" data-url="'.$url.'" href="'.$url.'" title="">Lease</a>';
		}
        
}

// add class to mailchimp newsletter form
add_filter( 'mc4wp_form_css_classes', function( $classes ) { 
	$classes[] = 'newsletter_subscribe';
	return $classes;
});


add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 100;
  return $cols;
}



