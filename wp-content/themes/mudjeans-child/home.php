<?php
get_header();
?>
		<main id="wrapper">
			<!-- de slider zal worden vervangen voor een slider plugin van Wordpress. Erwin Commijs weet welke dit moet worden -->
			<section id="slider">
				<div class="slider_overflow">
					<div class="backgroundsize slide" style="background-image:url(https://herschel.eu/content/dam/herschel/lifestyle/2018-s1/s1_18-launch/s1_18-category-banners/HSC-S1_18-Mens-Backpacks.jpg);">
						<div class="table">
							<div class="table_cell">
								<div class="center">
									<article class="slide_content black"><!-- de class 'black' maakt de titels zwart -->
										<h1>Sustainability at<br/> the core</h1>
										<p><a href="/winkel" title="">Shop now</a></p>
									</article>
								</div>
							</div>
						</div>
					</div>
					<div class="backgroundsize slide" style="background-image:url(/wp-content/themes/mudjeans-child/assets/images/slide.jpg);">
						<div class="table">
							<div class="table_cell">
								<div class="center">
									<article class="slide_content black"><!-- de class 'black' maakt de titels zwart -->
										<h1>Made to last</h1>
										<p><a href="/winkel" title="">Shop women</a></p>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<section class="the_page">
				<div class="layout_block content_quote">
					<div class="center_small">
						<div class="the_image quote">
							<figure class="visual left"><img src="/wp-content/themes/mudjeans-child/assets/images/boys.jpg" alt=""></figure>
							<blockquote class="right">Be kind to this world. Recycle <br/>your jeans.</blockquote>
						</div>
						<article class="the_content left">
							<h3>We believe</h3>
							<p>you can only enjoy life when next generations can enjoy this world too. You can make impact by purchasing the most sustainable jeans and recycle them when worn down.</p>
							<div class="button">
								<p><a href="/winkel" title="">Shop men</a> <a href="/winkel" title="">Shop women</a></p>
							</div>
						</article>
					</div>
				</div>
				
				<div class="layout_block images_quote">
					<div class="center_small">
						<div class="the_images quote">
							<div class="images_block right">
								<figure class="visual small left backgroundsize" style="background-image:url('/wp-content/themes/mudjeans-child/assets/images/plant.jpg');"><img src="/wp-content/themes/mudjeans-child/assets/images/plant.jpg" alt=""></figure>
								<figure class="visual large right backgroundsize" style="background-image:url('/wp-content/themes/mudjeans-child/assets/images/jeans.jpg');"><img src="/wp-content/themes/mudjeans-child/assets/images/jeans.jpg" alt=""></figure>
							</div>
							<blockquote class="left">Sustainability<br/> at<br/> the core</blockquote>
						</div>
						<article class="the_content right">
							<h3>Sustainability is</h3>
							<p>at the core of our company. Most of all we want the world to be a better place, thus we need a deeper understanding of the topic sustainability.</p>
							<div class="button">
								<p><a href="/editiorials" title="">Sustainability</a></p>
							</div>
						</article>
					</div>
				</div>
				
				<div class="layout_block products_quote">
					<div class="center">
						<div class="the_products left">
							<article class="product">
								<a href="/product/skinny-hazen-strong-blue/" title="">
									<div class="the_image">
										<figure class="visual visible"><img src="/wp-content/themes/mudjeans-child/assets/images/girl1.jpg" alt=""></figure>
										<figure class="visual backgroundsize" style="background-image:url('/wp-content/themes/mudjeans-child/assets/images/girl1z.jpg');"></figure>
									</div>
									<p>Skinny Hazen</p>
								</a>
							</article>
							<article class="product">
								<a href="/product/skinny-hazen-strong-blue/" title="">
									<div class="the_image">
										<figure class="visual visible"><img src="/wp-content/themes/mudjeans-child/assets/images/girl2.jpg" alt=""></figure>
										<figure class="visual backgroundsize" style="background-image:url('/wp-content/themes/mudjeans-child/assets/images/girl2z.jpg');"></figure>
									</div>
									<p>Skinny Hazen</p>
								</a>
							</article>
							<article class="product">
								<a href="/product/skinny-hazen-strong-blue/" title="">
									<div class="the_image">
										<figure class="visual visible"><img src="/wp-content/themes/mudjeans-child/assets/images/girl3.jpg" alt=""></figure>
										<figure class="visual backgroundsize" style="background-image:url('/wp-content/themes/mudjeans-child/assets/images/girl3z.jpg');"></figure>
									</div>
									<p>Skinny Hazen</p>
								</a>
							</article>
						</div>
						<blockquote class="right">"Wearing my Mud Jeans feels like making a statement that i do not support the fast fashion industry"</blockquote>
					</div>
				</div>
				
				<div class="selling_points">
					<div class="center_small">
						<ul>
							<li class="contact"><a href="/contact" title="">Contact us <small>hi there</small></a></li>
							<li class="returns"><a href="#" title="">Returns <small>15 days</small></a></li>
							<li class="shipping"><a href="#" title="">Shipping <small>1-4 days</small></a></li>
							<li class="lease"><a href="#" title="">Lease &amp; Buy <small>oh yes</small></a></li>
						</ul>
					</div>
				</div>
				
				<div class="layout_block quote_image">
					<div class="center_small">
						<div class="the_image right">
							<figure class="visual"><img src="/wp-content/themes/mudjeans-child/assets/images/insta.jpg" alt=""></figure>
						</div>
						
						<article class="the_content left">
							<blockquote>Instashop today</blockquote>
							<h3>A fair prices jeans that makes you feeld good</h3>
						</article>
					</div>
				</div>
				
				<div class="logo_block">
					<div class="center">
						<ul>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-1.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-2.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-3.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-4.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-5.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-6.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-7.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-8.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-9.png" alt=""></a></li>
							<li><a href="#" title=""><img src="/wp-content/themes/mudjeans-child/assets/images/logo-10.png" alt=""></a></li>
						</ul>
					</div>
				</div>
			</section>
		</main>
		
		<?php get_footer(); ?>