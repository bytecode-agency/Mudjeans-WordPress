<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Mudjeans
 */

?>
<!doctype html>
<html <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if(is_home()){
		echo '<link rel="stylesheet" type="text/css" href="/wp-content/themes/mudjeans-child/home.css">';
	}
		?>
	<?php wp_head();?>
</head>

<body <?php body_class();?>>


<?php global $woocommerce; ?>
<div id="page" class="site">
			<header id="header" class="
			<?php
			$frontpage_id = get_option( 'page_on_front' );
			if(!is_home() && !is_page($frontpage_id)){
				?>stay" style="position: relative !important;<?php //Fixes "fixed" position issues on pages that aren't the homepage
				} 
			?>
			">
				<a href="/" title="" class="logo">Mud Jeans</a>
				<nav id="nav">
				<?php wp_nav_menu(array('container'=> '','theme_location'=>'primary')); ?>
					<!--
                        <ul>
						<li><a href="#" title="">Women</a></li>
						<li><a href="#" title="">Men</a></li>
						<li><a href="#" title="">About &amp; lease</a></li>
						<li><a href="#" title="">Stores</a></li>
						<li><a href="#" title="">#Editorials</a></li>
                    </ul>
                    -->
				</nav>
				<nav id="meta">
					<ul>
						<li><a href="/mudjeans" title="">#Mudjeans</a></li>

						<li class="languages">
							<?php do_action('wpml_add_language_selector');  ?>
						</li>

						<?php if ( is_user_logged_in() ) { ?>
						 	<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title=""><?php _e('Mijn account','storefront'); ?></a></li>
						 <?php } 
						 else { ?>

						 	<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title=""><?php _e('Log in','storefront'); ?></a></li>

						<?php } ?>



						<li><a href="/contact" title="">Contact</a></li>
						<li class="shoppingcart">
							<a href="/winkelmand/" title=""><!--<span>2</span>--></a>
							<div class="total-cart-items">
								<?php echo WC()->cart->get_cart_contents_count(); ?>
							</div>
							<div class="shoppingcart-popup">
								<header><h3>In the bag</h3></header>

								<?php
								  
								    $items = $woocommerce->cart->get_cart();

								        foreach($items as $item => $values) { 
								            

								            //var_dump($values);

								            $productID = $values['product_id'];
								            $variation = $values['variation'];
								            $quantity = $values['quantity'];
								            $prod_title = get_field('product_titel', $productID);
								            $color_name = get_field('colour_name', $productID);
								            $price = get_post_meta( $productID, '_price', true);
								            $price_money = wc_price($price);

								            $product = wc_get_product($productID);
								            $productImg = $product->get_image(); 

								            $prod_size = '';
								            foreach ($variation as $size) {
								            	$prod_size = strtoupper($size);
								            }

								        	echo '<div class="group item"><div class="left col-half">';
								        	echo $productImg;
								        	echo '</div>';
								        	echo '<div class="right col-half">';
								        	echo '<p class="cart-prod-title"><strong>'. $prod_title.'<br>'.$price_money.'</strong></p>';
								        	echo '<p>Colour: '.$color_name.' <br> Qty: '.$quantity.' <br> Size: '.$prod_size.' </p>';
								        	echo '</div></div>';

								        } 
								
								        $cart_subtotal = $woocommerce->cart->get_cart_subtotal();
								?>

								<p class="cart-prod-subtotal"><strong>Subtotal: <?php echo $cart_subtotal; ?></strong></p>
								<div class="group">
									<div class="left col-half">

										<?php 

											
											$cart_url = wc_get_cart_url();
											$checkout_url = wc_get_checkout_url();

										 ?>
										<a class="button" href="<?php echo $cart_url; ?>" title="">View cart</a>
									</div>
									<div class="right col-half">
										<a class="button" href="<?php echo $checkout_url; ?>" title="">Checkout</a>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</nav>

				<button id="toggle" class="hidden" aria-hidden="true"><span></span></button>
			</header>
			<?php if(is_post_type_archive("product") || is_tax()){
					
				global $wp_query;
 				$current_termid = $wp_query->get_queried_object()->term_id; 

				$parentcats = get_ancestors($current_termid, 'product_cat');

				$firstParentCat = $parentcats[0];
				
				$term = get_term_by( 'id', $firstParentCat, 'product_cat', 'ARRAY_A' );

			?>
			<section id="intro_visual" class="backgroundsize" style="background-image:url(/wp-content/themes/mudjeans-child/assets/images/jeans-header.jpg); padding-bottom: 30px;">
				<h1 style="text-align: center; padding-top: 300px; color: white; font-size: 100px; line-height: 110px; letter-spacing: 0.3em; font-family: 'Book';"><?php echo $term['name']; ?></h1>
			</section>
			<?	
			}?>
	<!-- #masthead -->

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<?php
		/**
		 * Functions hooked in to storefront_content_top
		 *
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action( 'storefront_content_top' );
