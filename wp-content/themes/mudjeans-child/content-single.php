<?php
/**
 * Template used to display post content on single pages.
 *
 * @package storefront
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="blog_detail layout_block blog_layout the_first">
		<div class="center">
			<figure class="right visual"><img src="<?php the_post_thumbnail_url(); ?>" alt=""></figure>
			<article class="content left">
				<h1><?php the_title(); ?></h1>
				<time><?php the_date(); ?></time>
			</article>
		</div>
	</div>
	<div class="content">
		<?php the_content(); ?>
	</div>
</div><!-- #post-## -->
