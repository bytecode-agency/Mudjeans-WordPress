<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version' => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce = require 'inc/woocommerce/class-storefront-woocommerce.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/**
 * Add Custom Field for product variations: "Details"
 */
// -----------------------------------------
// 1. Add custom field input @ Product Data > Variations > Single Variation
  
add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations', 10, 3 ); 
 
function bbloomer_add_custom_field_to_variations( $loop, $variation_data, $variation ) {
woocommerce_wp_textarea_input( array( 
'id' => 'custom_field[' . $loop . ']',
'label' => 'Details tab',
'style' => 'display:  inline-block;',
'value' => get_post_meta( $variation->ID, 'custom_field', true )
) 
);      
}
  
// -----------------------------------------
// 2. Save custom field on product variation save
 
add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations', 10, 2 );
  
function bbloomer_save_custom_field_variations( $variation_id, $i ) {
    $custom_field = $_POST['custom_field'][$i];
    if( ! empty( $custom_field ) ) {
        update_post_meta( $variation_id, 'custom_field', esc_attr( $custom_field ) );
    }
}
  
// -----------------------------------------
// 3. Store custom field value into variation data
  
add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data' );
 
function bbloomer_add_custom_field_variation_data( $variations ) {
    $variations['custom_field'] = '<div class="woocommerce_custom_field">Custom Field: <span>' . get_post_meta( $variations[ 'variation_id' ], 'custom_field', true ) . '</span></div>';
    return $variations;
}

add_action('admin_head', 'style_label_custom_field');

function style_label_custom_field() {
  echo '<style>
  	p.form-field label {
      display: inline-block;
    } 
  </style>';
}

function register_my_menu() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
  }
  add_action( 'init', 'register_my_menu' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mudjeans_widgets_init() {
	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
		register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		) );
}
add_action( 'widgets_init', 'mudjeans_widgets_init' );

/*
* This Javascript adds WYSIWYG to the variable fields in Woocommerce
*/
function admin_add_wysiwyg_custom_field_textarea(){ 
?>
<script type="text/javascript">	
jQuery(function($){
	$(".variations_tab").click(function() {
		setTimeout(function(){
			$('.woocommerce_variation').each(function (index, value){
				$(this).addClass('variation-needs-update');
			});
			// Details
			detailsFields = 0;
			$('[id^=custom_field]').each(function (index, value){
				detailsFields++;
			});
			for(i = 0; i < detailsFields; i++){
				//var content = document.getElementById('custom_field['+ i +']').innerHTML
				tinyMCE.execCommand("mceAddEditor", false, 'custom_field['+ i +']');
				var activeEditor = tinyMCE.get('custom_field['+ i +']');
				if(activeEditor != null){
					//activeEditor.getBody().innerHTML = "does it work?";	
				}
			}
		}, 1000);
	});
});
jQuery( document ).ready( function( $ ) {
    $( '#post' ).submit( function( e ) {
		tinyMCE.triggerSave();
		// Details
		detailsFields = 0;
		$('[id^=custom_field]').each(function (index, value){
			detailsFields++;
		});
		for(i = 0; i < detailsFields; i++){
			$('#custom_field['+ i +']').html(text);
			var activeEditor = tinyMCE.get('custom_field['+ i +']');
			if(activeEditor != null){
				var text = activeEditor.getContent();
				if(text != "undefined"){
					$("textarea[name='custom_field["+ i +"]").text(text);
				}
			}
		}
		// Description
		descriptionFields = 0;
		$('[id^=variable_description]').each(function (index, value){
			descriptionFields++;
		});
		for(j = 0; j < descriptionFields; j++){
			var activeEditor = tinyMCE.get('variable_description'+ j);
			if(activeEditor != null){
				var text = activeEditor.getConte
				nt();
				if(text != null){
					$("textarea[name='variable_description["+ j +"]").text(text);
				}
			}
		}
		setTimeout(() => {
			return true;
		}, 3000);
    });
});
</script>
	<?php
}
add_action( 'admin_print_footer_scripts', 'admin_add_wysiwyg_custom_field_textarea', 99 );