<?php

/**
 * Fired during plugin activation
 *
 * @link       Buro Zero
 * @since      1.0.0
 *
 * @package    Bz
 * @subpackage Bz/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bz
 * @subpackage Bz/includes
 * @author     Buro Zero <jeroen@burozero.com>
 */
class Bz_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
