<?php

/**
 * Fired during plugin deactivation
 *
 * @link       Buro Zero
 * @since      1.0.0
 *
 * @package    Bz
 * @subpackage Bz/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Bz
 * @subpackage Bz/includes
 * @author     Buro Zero <jeroen@burozero.com>
 */
class Bz_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
