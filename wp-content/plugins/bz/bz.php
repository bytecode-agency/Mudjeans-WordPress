<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              Buro Zero
 * @since             1.0.0
 * @package           Bz
 *
 * @wordpress-plugin
 * Plugin Name:       WYSIWYG Buro Zero
 * Plugin URI:        bz
 * Description:       Adds WYSIWYG editors to the Woocommerce Product Variations
 * Version:           1.0.0
 * Author:            Buro Zero
 * Author URI:        Buro Zero
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bz
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-bz-activator.php
 */
function activate_bz() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bz-activator.php';
	Bz_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-bz-deactivator.php
 */
function deactivate_bz() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bz-deactivator.php';
	Bz_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_bz' );
register_deactivation_hook( __FILE__, 'deactivate_bz' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-bz.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_bz() {

	$plugin = new Bz();
	$plugin->run();

	/**
	 * Add Custom Field for product variations: "Details"
	 */
	// -----------------------------------------
	// 1. Add custom field input @ Product Data > Variations > Single Variation
	
	add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations', 10, 3 ); 
	
	function bbloomer_add_custom_field_to_variations( $loop, $variation_data, $variation ) {
	woocommerce_wp_textarea_input( array( 
	'id' => 'custom_field[' . $loop . ']',
	'label' => 'Details tab',
	'style' => 'display:  inline-block;',
	'value' => get_post_meta( $variation->ID, 'custom_field', true )
	) 
	);      
	}
	
	// -----------------------------------------
	// 2. Save custom field on product variation save
	
	add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations', 10, 2 );
	
	function bbloomer_save_custom_field_variations( $variation_id, $i ) {
		$custom_field = $_POST['custom_field'][$i];
		if( ! empty( $custom_field ) ) {
			update_post_meta( $variation_id, 'custom_field', esc_attr( $custom_field ) );
		}
	}
	
	// -----------------------------------------
	// 3. Store custom field value into variation data
	
	add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data' );
	
	function bbloomer_add_custom_field_variation_data( $variations ) {
		$variations['custom_field'] = '<div class="woocommerce_custom_field">Custom Field: <span>' . get_post_meta( $variations[ 'variation_id' ], 'custom_field', true ) . '</span></div>';
		return $variations;
	}

	add_action('admin_head', 'style_label_custom_field');

	function style_label_custom_field() {
	echo '<style>
		p.form-field label {
		display: inline-block;
		} 
	</style>';
	}

	/*
	* WYSIWYG
	*/
	function admin_add_wysiwyg_custom_field_textarea()
	{ ?>
	<script type="text/javascript">	
	jQuery(function($){
		function htmlDecode(value){
  			return $('<div/>').html(value).text();
		}
		$(".variations_tab").click(function() {
		setTimeout(function(){
			tinymce.init({selector:'textarea'});
			// Variations need to be updated.
			$('.woocommerce_variation').each(function (index, value){
				$(this).addClass('variation-needs-update');
			});
			// Count the number of details fields
			detailsFields = 0;
			$('[id^=custom_field]').each(function (index, value){
				detailsFields++;
			});
			// Count the number of description fields
			descFields = 0;
			$('[id^=variable_description]').each(function (index, value){
				descFields++;
			});
			// Loop over them
			for(i = 0; i < detailsFields - 1; i++){
				var activeEditor = tinyMCE.get('custom_field['+ i +']');
				if(activeEditor != null){
					var text = activeEditor.getContent();
					if(text != "undefined"){ 
						var html = htmlDecode(text);
						activeEditor.setContent(html, {format: 'raw'});
					}
				}
			}
			for(j = 0; j < descFields - 1; j++){
				var activeEditor = tinyMCE.get('variable_description'+ j);
				if(activeEditor != null){
					var text = activeEditor.getContent();
					if(text != "undefined"){ 
						activeEditor.setContent(text, {format: 'raw'});
					}
				}
			}
		}, 1000);
		});
	});
	jQuery( document ).ready( function( $ ) {
    $( '.save-variation-changes' ).click( function( e ) {
		// Details
		detailsFields = 0;
		$('[id^=custom_field]').each(function (index, value){
			detailsFields++;
		});
		for(i = 0; i < detailsFields - 1; i++){
			var activeEditor = tinyMCE.get('custom_field['+ i +']');
			if(activeEditor != null){
				var text = activeEditor.getContent();
				if(text != "undefined"){
					$("textarea[name='custom_field["+ i +"]").text(text);
				}
			}
		}
		for(j = 0; j < detailsFields - 1; j++){
			var activeEditor = tinyMCE.get('variable_description'+ j);
			if(activeEditor != null){
				var text = activeEditor.getContent();
				if(text != "undefined"){
					$("textarea[name='variable_description["+ j +"]").text(text);
				}
			}
		}
    });
});
	</script>
	<?php }
	add_action( 'admin_print_footer_scripts', 'admin_add_wysiwyg_custom_field_textarea', 99 );

}
run_bz();
